package taxidermist

import (
	"github.com/jackmanlabs/errors"
	"html/template"
	"io"
	"net/http"
	"os"
	"strings"
)

var (
	DefaultBasePath  = "./templates/"
	DefaultPageTitle = ""
	SiteTitle        = ""

	// You might consider changing the delimiters if you are using something like AngularJS.
	DelimLeft  = "{{"
	DelimRight = "}}"
)

type Taxidermist struct {
	SiteTitle string
	PageTitle string
	Content   interface{}

	// Control variables
	templatePaths    []string
	templateBasePath string
	template         *template.Template
}

func New() *Taxidermist {
	t := new(Taxidermist)
	t.PageTitle = DefaultPageTitle
	t.SiteTitle = SiteTitle
	t.templateBasePath = DefaultBasePath

	return t
}

// Returns an error iff the template path does not exist.
func (this *Taxidermist) SetTemplatePathManual(path string) error {
	// Create a list of directories from which we'll pull templates.
	chunks_raw := strings.Split(path, "/")
	chunks := make([]string, 0)

	// By processing this list, we're eliminating double slashes and trailing empties.
	for _, chunk := range chunks_raw {
		chunk = strings.TrimSpace(chunk)
		if chunk != "" {
			chunks = append(chunks, chunk)
		}
	}

	this.templatePaths = make([]string, 0)
	for i := len(chunks); i >= 0; i-- {
		path := this.templateBasePath
		subchunks := chunks[0:i]
		for _, chunk := range subchunks {
			path += chunk + "/"
		}
		this.templatePaths = append(this.templatePaths, path)
	}

	return nil
}

// Sets the template path according to the URL.
// Returns an error iff the template path does not exist.
func (this *Taxidermist) SetTemplatePathURL(r *http.Request) error {
	// Create a list of directories from which we'll pull templates.
	chunks_raw := strings.Split(r.URL.Path, "/")
	chunks := make([]string, 0)

	//	log.Println("RAW CHUNKS:")
	//	bucket.JLog(chunks_raw)

	// By processing this list, we're eliminating double slashes and trailing empties.
	for _, chunk := range chunks_raw {
		chunk = strings.TrimSpace(chunk)
		if chunk != "" {
			chunks = append(chunks, chunk)
		}
	}

	//	log.Println("CHUNKS:")
	//	bucket.JLog(chunks)

	this.templatePaths = make([]string, 0)
	for i := len(chunks); i >= 0; i-- {
		path := this.templateBasePath
		subchunks := chunks[0:i]
		for _, chunk := range subchunks {
			path += chunk + "/"
		}
		this.templatePaths = append(this.templatePaths, path)
	}

	//log.Println("PATHS:")
	//jlog.Log(this.templatePaths)

	return nil
}

func (this *Taxidermist) Present(w io.Writer) error {
	var err error

	// As far as I can tell the name is arbitrary.
	this.template = template.New("")
	this.template.Delims(DelimLeft, DelimRight)

	for _, tdir := range this.templatePaths {
		if err := this.loadPath(tdir); err != nil {
			return errors.Stack(err)
		}
	}

	// Now we can execute the template.
	// (output writer, template name, data)
	if err = this.template.ExecuteTemplate(w, "main.html", this); err != nil {
		return errors.Stack(err)
	}

	return nil
}

// Loads any templates from the given directory into the given template.
func (this *Taxidermist) loadPath(path string) error {
	dir, err := os.Open(path)
	if os.IsNotExist(err) {
		// Do nothing. Ignore URL components that don't exist.
		return nil
	} else if err != nil {
		return errors.Stack(err)
	}

	fileInfos, err := dir.Readdir(0)
	if err != nil {
		return errors.Stack(err)
	}

	var templates []string

	for _, value := range this.template.Templates() {
		templates = append(templates, value.Name())
	}

	// Go over all the files in the directory, and add any 'html' files to the template.
	for _, fileInfo := range fileInfos {
		if strings.HasSuffix(fileInfo.Name(), ".html") {
			if !contains(templates, fileInfo.Name()) {
				//log.Println(fileInfo.Name())
				this.template.ParseFiles(path + fileInfo.Name())
			}
		}
	}

	return nil
}
