tmaster
=======

Helper to take care of the busywork of using standard library templates.

I really like the usability and flexibility of using the standard library templates, but I realized pretty quickly how repetitive my code was getting. This is simply the result of my attempts to clean up my code.
